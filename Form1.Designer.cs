﻿namespace TestTaskDircetum
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.infoLabelRkk = new System.Windows.Forms.Label();
            this.infoLabelApp = new System.Windows.Forms.Label();
            this.RkkPathLabel = new System.Windows.Forms.Label();
            this.AppPathLabel = new System.Windows.Forms.Label();
            this.chooseRkkFileButton = new System.Windows.Forms.Button();
            this.chooseAppealFileButton = new System.Windows.Forms.Button();
            this.table1 = new System.Windows.Forms.DataGridView();
            this.n = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rkk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.app = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.infoLabelSort = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.rkkSearchTimeLabel = new System.Windows.Forms.Label();
            this.appSearchTimeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.table1)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // infoLabelRkk
            // 
            this.infoLabelRkk.AutoSize = true;
            this.infoLabelRkk.BackColor = System.Drawing.SystemColors.Control;
            this.infoLabelRkk.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoLabelRkk.Location = new System.Drawing.Point(20, 11);
            this.infoLabelRkk.Name = "infoLabelRkk";
            this.infoLabelRkk.Size = new System.Drawing.Size(102, 19);
            this.infoLabelRkk.TabIndex = 0;
            this.infoLabelRkk.Text = "Файл с РКК:";
            // 
            // infoLabelApp
            // 
            this.infoLabelApp.AutoSize = true;
            this.infoLabelApp.BackColor = System.Drawing.SystemColors.Control;
            this.infoLabelApp.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoLabelApp.Location = new System.Drawing.Point(20, 77);
            this.infoLabelApp.Name = "infoLabelApp";
            this.infoLabelApp.Size = new System.Drawing.Size(192, 19);
            this.infoLabelApp.TabIndex = 1;
            this.infoLabelApp.Text = "Файл с обращениями:";
            // 
            // RkkPathLabel
            // 
            this.RkkPathLabel.AutoSize = true;
            this.RkkPathLabel.BackColor = System.Drawing.SystemColors.Control;
            this.RkkPathLabel.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RkkPathLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.RkkPathLabel.Location = new System.Drawing.Point(128, 12);
            this.RkkPathLabel.Name = "RkkPathLabel";
            this.RkkPathLabel.Size = new System.Drawing.Size(79, 19);
            this.RkkPathLabel.TabIndex = 2;
            this.RkkPathLabel.Text = "Не выбран";
            // 
            // AppPathLabel
            // 
            this.AppPathLabel.AutoSize = true;
            this.AppPathLabel.BackColor = System.Drawing.SystemColors.Control;
            this.AppPathLabel.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AppPathLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.AppPathLabel.Location = new System.Drawing.Point(218, 78);
            this.AppPathLabel.Name = "AppPathLabel";
            this.AppPathLabel.Size = new System.Drawing.Size(79, 19);
            this.AppPathLabel.TabIndex = 3;
            this.AppPathLabel.Text = "Не выбран";
            // 
            // chooseRkkFileButton
            // 
            this.chooseRkkFileButton.BackColor = System.Drawing.Color.DarkOrange;
            this.chooseRkkFileButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.chooseRkkFileButton.FlatAppearance.BorderSize = 0;
            this.chooseRkkFileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chooseRkkFileButton.Font = new System.Drawing.Font("Corbel", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chooseRkkFileButton.ForeColor = System.Drawing.Color.White;
            this.chooseRkkFileButton.Location = new System.Drawing.Point(24, 33);
            this.chooseRkkFileButton.Name = "chooseRkkFileButton";
            this.chooseRkkFileButton.Size = new System.Drawing.Size(75, 30);
            this.chooseRkkFileButton.TabIndex = 4;
            this.chooseRkkFileButton.Text = "ОБЗОР";
            this.chooseRkkFileButton.UseVisualStyleBackColor = false;
            this.chooseRkkFileButton.Click += new System.EventHandler(this.chooseRkkFileButton_Click);
            // 
            // chooseAppealFileButton
            // 
            this.chooseAppealFileButton.BackColor = System.Drawing.Color.DarkOrange;
            this.chooseAppealFileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chooseAppealFileButton.Font = new System.Drawing.Font("Corbel", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chooseAppealFileButton.ForeColor = System.Drawing.Color.White;
            this.chooseAppealFileButton.Location = new System.Drawing.Point(24, 99);
            this.chooseAppealFileButton.Name = "chooseAppealFileButton";
            this.chooseAppealFileButton.Size = new System.Drawing.Size(75, 30);
            this.chooseAppealFileButton.TabIndex = 6;
            this.chooseAppealFileButton.Text = "ОБЗОР";
            this.chooseAppealFileButton.UseVisualStyleBackColor = false;
            this.chooseAppealFileButton.Click += new System.EventHandler(this.chooseAppealFileButton_Click);
            // 
            // table1
            // 
            this.table1.AllowUserToAddRows = false;
            this.table1.AllowUserToDeleteRows = false;
            this.table1.AllowUserToResizeColumns = false;
            this.table1.AllowUserToResizeRows = false;
            this.table1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.table1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.table1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.MenuText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.table1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.table1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.n,
            this.name,
            this.rkk,
            this.app,
            this.total});
            this.table1.Location = new System.Drawing.Point(24, 163);
            this.table1.Name = "table1";
            this.table1.Size = new System.Drawing.Size(537, 275);
            this.table1.TabIndex = 9;
            // 
            // n
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.n.DefaultCellStyle = dataGridViewCellStyle2;
            this.n.HeaderText = "№ п.п.";
            this.n.Name = "n";
            this.n.ReadOnly = true;
            this.n.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // name
            // 
            this.name.HeaderText = "Ответственный исполнитель";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // rkk
            // 
            this.rkk.HeaderText = "Количество неисполненных входящих документов";
            this.rkk.Name = "rkk";
            this.rkk.ReadOnly = true;
            this.rkk.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // app
            // 
            this.app.HeaderText = "Количество неисполненных письменных обращений граждан";
            this.app.Name = "app";
            this.app.ReadOnly = true;
            this.app.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // total
            // 
            this.total.HeaderText = "Общее количество документов и обращений";
            this.total.Name = "total";
            this.total.ReadOnly = true;
            this.total.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "по фамилии",
            "по ркк",
            "по обращениям",
            "по общему кол-ву док-ов"});
            this.comboBox1.Location = new System.Drawing.Point(144, 136);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(170, 21);
            this.comboBox1.TabIndex = 10;
            this.comboBox1.Text = "по общему кол-ву док-ов";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.TextUpdate += new System.EventHandler(this.comboBox1_TextUpdate);
            // 
            // infoLabelSort
            // 
            this.infoLabelSort.AutoSize = true;
            this.infoLabelSort.BackColor = System.Drawing.SystemColors.Control;
            this.infoLabelSort.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoLabelSort.Location = new System.Drawing.Point(21, 138);
            this.infoLabelSort.Name = "infoLabelSort";
            this.infoLabelSort.Size = new System.Drawing.Size(117, 16);
            this.infoLabelSort.TabIndex = 11;
            this.infoLabelSort.Text = "Отсортировать:";
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.DarkOrange;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.Font = new System.Drawing.Font("Corbel", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.saveButton.ForeColor = System.Drawing.Color.White;
            this.saveButton.Location = new System.Drawing.Point(461, 134);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(100, 23);
            this.saveButton.TabIndex = 12;
            this.saveButton.Text = "СОХРАНИТЬ";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // rkkSearchTimeLabel
            // 
            this.rkkSearchTimeLabel.AutoSize = true;
            this.rkkSearchTimeLabel.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rkkSearchTimeLabel.Location = new System.Drawing.Point(105, 39);
            this.rkkSearchTimeLabel.Name = "rkkSearchTimeLabel";
            this.rkkSearchTimeLabel.Size = new System.Drawing.Size(110, 19);
            this.rkkSearchTimeLabel.TabIndex = 13;
            this.rkkSearchTimeLabel.Text = "Время поиска:";
            // 
            // appSearchTimeLabel
            // 
            this.appSearchTimeLabel.AutoSize = true;
            this.appSearchTimeLabel.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.appSearchTimeLabel.Location = new System.Drawing.Point(105, 105);
            this.appSearchTimeLabel.Name = "appSearchTimeLabel";
            this.appSearchTimeLabel.Size = new System.Drawing.Size(110, 19);
            this.appSearchTimeLabel.TabIndex = 14;
            this.appSearchTimeLabel.Text = "Время поиска:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 484);
            this.Controls.Add(this.appSearchTimeLabel);
            this.Controls.Add(this.rkkSearchTimeLabel);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.infoLabelSort);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.table1);
            this.Controls.Add(this.chooseAppealFileButton);
            this.Controls.Add(this.chooseRkkFileButton);
            this.Controls.Add(this.AppPathLabel);
            this.Controls.Add(this.RkkPathLabel);
            this.Controls.Add(this.infoLabelApp);
            this.Controls.Add(this.infoLabelRkk);
            this.MaximumSize = new System.Drawing.Size(600, 800);
            this.Name = "Form1";
            this.Text = "DirectumTestTask";
            ((System.ComponentModel.ISupportInitialize)(this.table1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label infoLabelRkk;
        private System.Windows.Forms.Label infoLabelApp;
        private System.Windows.Forms.Label RkkPathLabel;
        private System.Windows.Forms.Label AppPathLabel;
        private System.Windows.Forms.Button chooseRkkFileButton;
        private System.Windows.Forms.Button chooseAppealFileButton;
        private System.Windows.Forms.DataGridView table1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label infoLabelSort;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn n;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn rkk;
        private System.Windows.Forms.DataGridViewTextBoxColumn app;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.Label rkkSearchTimeLabel;
        private System.Windows.Forms.Label appSearchTimeLabel;
    }
}

