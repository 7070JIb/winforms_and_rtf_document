﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.Office.Interop.Word;

namespace TestTaskDircetum
{
    public partial class Form1 : Form
    {
        Dictionary<string, int[]> docsPerPerson = new Dictionary<string, int[]>();
        bool[] AreFilesAdded = new bool[2];
        Label[] txtPathLabels;
        Label[] timeLabels;

        string lastComboBoxText;

        public Form1()
        {
            InitializeComponent();

            txtPathLabels = new Label[] { RkkPathLabel, AppPathLabel };
            timeLabels = new Label[] { rkkSearchTimeLabel, appSearchTimeLabel };
            timeLabels[0].Visible = false;
            timeLabels[1].Visible = false;
            lastComboBoxText = comboBox1.Text;

            table1.RowHeadersDefaultCellStyle.BackColor = System.Drawing.Color.DarkOrange;
        }

        private void chooseRkkFileButton_Click(object sender, EventArgs e) => ReadAndProccessFile(0);

        private void chooseAppealFileButton_Click(object sender, EventArgs e) => ReadAndProccessFile(1);

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lastComboBoxText = comboBox1.Text;
            UpdateTable(comboBox1.SelectedIndex);
        }

        private void comboBox1_TextUpdate(object sender, EventArgs e)
        {
            comboBox1.Text = lastComboBoxText;
        }

        private void saveButton_Click(object sender, EventArgs e) =>  CreateAndSaveFile();

        private void UpdateTable(int sortingType)
        {
            IOrderedEnumerable<KeyValuePair<string, int[]>> sortedDictionary;
            switch (sortingType)
            {
                case 0:
                    sortedDictionary = SortSecondName(docsPerPerson);
                    break;
                case 1:
                    sortedDictionary = SortRKK(docsPerPerson);
                    break;
                case 2:
                    sortedDictionary = SortAppeals(docsPerPerson);
                    break;
                case 3:
                    sortedDictionary = SortTotal(docsPerPerson);
                    break;
                default:
                    throw new ArgumentNullException(nameof(sortedDictionary));
            }

            table1.Rows.Clear();

            int i = 1;
            foreach (var pair in sortedDictionary)
                table1.Rows.Add(i++, pair.Key, pair.Value[0], pair.Value[1], pair.Value[2]);
        }

        #region РАБОТА С TXT
        /// <summary>
        /// Вызов диалога, чтение файла, парсинг строк, запись в словарь
        /// </summary>
        /// <param name="numericField">Значения параметра: 0 - учет РКК, 1 - учет обращений</param>
        private void ReadAndProccessFile(int numericField)
        {

            //openFileDialog1.Filter = "*txt";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                DateTime start = DateTime.Now;
                // Вывести путь файла.
                txtPathLabels[numericField].Text = openFileDialog1.FileName.Remove(0, openFileDialog1.FileName.LastIndexOf('\\') + 1);
                txtPathLabels[numericField].ForeColor = System.Drawing.Color.DarkOrange;

                // Обнулить старые данные
                ClearDocsData(numericField);
                
                try
                {
                    using (StreamReader reader = new StreamReader(openFileDialog1.OpenFile()))
                    {
                        while (!reader.EndOfStream)
                        {
                            string responsible = FindResponsible(reader.ReadLine());

                            if (!docsPerPerson.ContainsKey(responsible))
                                docsPerPerson.Add(responsible, new int[3]);

                            docsPerPerson[responsible][numericField]++;
                            docsPerPerson[responsible][2]++;
                        }
                    }

                    AreFilesAdded[numericField] = true;
                    DateTime stop = DateTime.Now;
                    int msDifference = stop.Subtract(start).Milliseconds;

                    if (!timeLabels[numericField].Visible)
                        timeLabels[numericField].Visible = true;

                    timeLabels[numericField].Text = "Время поиска: " + msDifference + " ms";

                }
                catch
                {
                    if (!timeLabels[numericField].Visible)
                        timeLabels[numericField].Visible = true;

                    timeLabels[numericField].Text = "Ошибка при чтении файла";
                    ClearDocsData(numericField);
                }
                finally
                {
                    UpdateTable(comboBox1.SelectedIndex);
                }

            }

        }

        private void ClearDocsData(int numericField)
        {
            if (AreFilesAdded[numericField])
            {
                foreach (var field in docsPerPerson)
                {
                    field.Value[2] -= field.Value[numericField];
                    field.Value[numericField] = 0;
                }
            }
        }

        /// <summary>
        /// Парсинг строки
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        private string FindResponsible(string rawLine)
        {
            string[] line = rawLine.Split('\t');
            if (line[0] == "Климов Сергей Александрович")
            {
                string responsible = line[1].Split(';').FirstOrDefault();
                if (responsible.Contains(" (Отв.)"))
                    responsible = responsible.Remove(responsible.IndexOf(" (Отв.)"), " (Отв.)".Length);

                return responsible;
            }
            else
            {
                string[] fullName = line[0].Split(' ');

                return string.Format($"{fullName[0]} {fullName[1][0]}.{fullName[2][0]}.");
            }
        }

        #endregion

        #region СОРТИРОВКИ

        // По Фамилии
        private IOrderedEnumerable<KeyValuePair<string, int[]>> SortSecondName(Dictionary<string, int[]> dictionary)
            => dictionary.OrderBy(t => t.Key);

        // По РКК
        private IOrderedEnumerable<KeyValuePair<string, int[]>> SortRKK(Dictionary<string, int[]> dictionary)
            => dictionary.OrderByDescending(t => t.Value[0]).ThenByDescending(t => t.Value[1]);

        // По обращениям
        private IOrderedEnumerable<KeyValuePair<string, int[]>> SortAppeals(Dictionary<string, int[]> dictionary)
            => dictionary.OrderByDescending(t => t.Value[1]).ThenByDescending(t => t.Value[0]);

        // По общему количеству док-ов
        private IOrderedEnumerable<KeyValuePair<string, int[]>> SortTotal(Dictionary<string, int[]> dictionary)
            => dictionary.OrderByDescending(t => t.Value[2]).ThenByDescending(t => t.Value[0]);



        #endregion

        private void CreateAndSaveFile()
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                int rkkSum = docsPerPerson.Sum(t => t.Value[0]);
                int appSum = docsPerPerson.Sum(t => t.Value[1]);
                int totalSum = docsPerPerson.Sum(t => t.Value[2]);
                string sortType = comboBox1.Text;
                string date = string.Format(DateTime.Now.ToString("dd.MM.yyyy"));

                object missing = System.Reflection.Missing.Value;
                object oEndOfDoc = "\\endofdoc";


                _Application word = new Microsoft.Office.Interop.Word.Application() { Visible = false };

                _Document doc = word.Documents.Add(ref missing, ref missing, ref missing, ref missing);

                Paragraph par = doc.Paragraphs.Add(ref missing);
                par.Range.Text = "Справка о неисполненных документах и обращениях граждан";
                par.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                par.Format.SpaceAfter = 0;
                par.Format.SpaceBefore = 0;
                par.Range.Bold = 1;
                par.Range.Font.Size = 14;
                par.Range.Font.Name = "Arial";
                par.Range.InsertParagraphAfter();
                par.Range.InsertParagraphAfter();

                Range rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                par.Range.Font.Size = 10;
                par.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;
                par.Range.Bold = 0;
                rng.Text = "Не исполнено в срок ";

                // totalSum
                doc.Bookmarks.Add("totalSum", doc.Bookmarks.get_Item(ref oEndOfDoc).Range);

                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                rng.Text = " документов, из них:";
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                rng.InsertParagraph();
                rng.InsertParagraphAfter();
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                rng.InsertAfter("- количество неисполненных входящих документов: ");
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                // rkkSum
                doc.Bookmarks.Add("rkkSum", doc.Bookmarks.get_Item(ref oEndOfDoc).Range);
                rng.InsertAfter(";");
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                rng.InsertParagraph();
                rng.InsertParagraphAfter();
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                rng.InsertAfter("- количество неисполненных письменных обращений граждан: ");
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                // appSum
                doc.Bookmarks.Add("appSum", doc.Bookmarks.get_Item(ref oEndOfDoc).Range);
                rng.InsertAfter(";");
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                rng.InsertParagraph();
                rng.InsertParagraphAfter();
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                rng.InsertAfter("Сортировка: ");
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                // sortType
                doc.Bookmarks.Add("sortType", doc.Bookmarks.get_Item(ref oEndOfDoc).Range);
                rng.InsertAfter(".");
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;

                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                Table table = doc.Tables.Add(rng, docsPerPerson.Count()+1, 5, ref missing, ref missing);
                table.Rows.Alignment = WdRowAlignment.wdAlignRowCenter;
                table.Borders.Enable = 1;
                table.Range.Borders.OutsideColor = WdColor.wdColorBlack;
                for (int i = 1; i <= table.Rows[1].Cells.Count; i++)
                {
                    Cell cell = table.Rows[1].Cells[i];
                    cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                    cell.Range.Text = table1.Columns[i - 1].HeaderText;
                    cell.Range.Font.Bold = 1;
                }
                for (int i = 2; i <= table.Rows.Count; i++)
                {
                    for (int j = 1; j <= table.Rows[i].Cells.Count; j++)
                    {
                        table.Cell(i, j).Range.Text = table1.Rows[i-2].Cells[j-1].Value.ToString();
                        if (j != 2) table.Cell(i, j).Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                    }
                }
                table.Columns[1].AutoFit();
                table.Columns[2].AutoFit();

                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                rng.InsertParagraph();
                rng.InsertParagraphAfter();
                rng = doc.Bookmarks.get_Item(ref oEndOfDoc).Range;
                rng.Text = "Дата составления справки: ";

                // date
                doc.Bookmarks.Add("date", doc.Bookmarks.get_Item(ref oEndOfDoc).Range);


                // Переменные
                rng = doc.Bookmarks.get_Item("totalSum").Range;
                rng.Text = totalSum.ToString();
                rng.Bold = 1;
                rng.Font.Size = 10;
                rng = doc.Bookmarks.get_Item("rkkSum").Range;
                rng.Text = rkkSum.ToString();
                rng.Bold = 1;
                rng.Font.Size = 10;
                rng = doc.Bookmarks.get_Item("appSum").Range;
                rng.Text = appSum.ToString();
                rng.Bold = 1;
                rng.Font.Size = 10;
                rng = doc.Bookmarks.get_Item("sortType").Range;
                rng.Text = sortType;
                rng.Bold = 1;
                rng.Font.Size = 10;
                rng = doc.Bookmarks.get_Item("date").Range;
                rng.Text = date;
                rng.Bold = 1;
                rng.Font.Size = 10;

                doc.SaveAs2(saveFileDialog1.FileName, WdSaveFormat.wdFormatRTF);
                doc.Close();
                word.Quit();
            }
        }

    }
}
